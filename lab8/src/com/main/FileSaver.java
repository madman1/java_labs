package com.main;

import java.io.*;
import java.nio.file.*;
import java.sql.*;

public class FileSaver
{
    private String _dbUrl;
    private String _user;
    private String _password;
    private static final int BUFFER_SIZE = 4096;

    public FileSaver(String dbUrl, String user, String password)
    {
        _dbUrl = dbUrl;
        _user = user;
        _password = password;
    }

    public void Save(String fileName, InputStream inputStream) throws SQLException
    {
        try
        {
            Connection conn = DriverManager.getConnection(_dbUrl, _user, _password);

            String sql = "INSERT INTO file (name, document) values (?, ?)";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, fileName);


            statement.setBlob(2, inputStream);

            int row = statement.executeUpdate();
            conn.close();
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    public void Read(String fileName, OutputStream outputStream) throws SQLException, IOException
    {
        try {
            Connection conn = DriverManager.getConnection(_dbUrl, _user, _password);

            String sql = "SELECT document FROM file WHERE name=?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, fileName);

            ResultSet result = statement.executeQuery();
            if (result.next()) {
                Blob blob = result.getBlob("document");
                InputStream inputStream = blob.getBinaryStream();

                int bytesRead = -1;
                byte[] buffer = new byte[BUFFER_SIZE];
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                inputStream.close();
                outputStream.close();
                System.out.println("File saved");
            }
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        } catch (IOException ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    public boolean Delete(String fileName)
    {
        try
        {
            Connection conn = DriverManager.getConnection(_dbUrl, _user, _password);

            String sql = "DELETE FROM file WHERE name=?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, fileName);

            boolean res = statement.execute();
            conn.close();

            return  res;
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
        }

        return  false;
    }
}