package com.main.test;

import com.main.FileSaver;
import org.junit.Assert;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by taras_000 on 13-Nov-16.
 */
public class FileSaverTest {

    FileSaver _saver;

    @org.junit.Before
    public void setUp() throws Exception
    {
        String url = "jdbc:mysql://localhost:3306/io";

        String user = "root";
        String password = "1111";

        _saver = new FileSaver(url, user, password);
    }

    @org.junit.Test
    public void save() throws Exception
    {
        //delete file from db
       _saver.Delete("dbTest.txt");

        String path = System.getProperty("user.dir");

        File f = new File(path + "/src/com/main/test/dbTest.txt");
        String fileName = f.getName();
        InputStream inputStream = new FileInputStream(f);
        _saver.Save(fileName, inputStream);

        String outFile = path + "/src/com/main/test/dbOut.txt";
        OutputStream outputStream = new FileOutputStream(outFile);
        _saver.Read("dbTest.txt", outputStream);

        Path filePath = FileSystems.getDefault().getPath(outFile);
        List<String> result = Files.readAllLines(filePath);

        assertTrue(result.get(0).equals("Hello"));
        assertTrue(result.get(1).equals("World!"));
    }
}