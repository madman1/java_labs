package com.main;

import java.awt.font.NumericShaper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {
        GetPrimeByOnes(7);
    }

    /**
     * Returns prime int number with the max number of 1 in binary form
     * @param maxNumber max number of the sequence
     * @return prime int
     */
    public static int GetPrimeByOnes(int maxNumber)
    {
        if(maxNumber <= 1)
        {
            throw new IllegalArgumentException("maxNumber is lower than the lowes pime number");
        }

        //boolean[] allNumbers = new boolean[maxNumber+1];
        List<Boolean> allNumbers = IntStream.range(0, maxNumber + 1).mapToObj(x-> true).collect(Collectors.toList());

        IntStream.range(2, maxNumber + 1).forEach((i)->
        {
            if(Math.pow(i, 2) <= maxNumber && allNumbers.get(i) == true)
            {
                for (int j = (int)Math.pow(i, 2); j<=maxNumber; j+=i)
                {
                    allNumbers.set(j, false);
                }
            }
        });

        int result = IntStream.range(2, maxNumber + 1).map(i-> {
            if(allNumbers.get(i))
            {
                return i;
            }
            return -1;
        }).filter(x-> x != -1).mapToObj(n ->{
            Result r = new Result();
            r.Number = n;
            r.CountOfOne = Integer.toBinaryString(n).replace("0", "").length();
            return r;
        }).max(Comparator.comparing((x)-> x.CountOfOne)).map(x-> x.Number).get();

        return result;
    }
}

