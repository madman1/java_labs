package com.main;

public class Producer implements Runnable
{
    private BlockingQueue _queue = null;

    public Producer(BlockingQueue queue) {
        _queue = queue;
    }

    public void run() {
        try {
            long n = Thread.currentThread().getId();

            while (true)
            {
                _queue.put("Потік №" + n + " згенерував повідомлення");
                Thread.sleep(100);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
