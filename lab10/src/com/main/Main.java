package com.main;

public class Main {

    public static void main(String[] args)
    {
        BlockingQueue queue = new BlockingQueue(20);
        BlockingQueue resultQueue = new BlockingQueue(20);

        Producer producer = new Producer(queue);
        Consumer consumer = new Consumer(queue, resultQueue);

        Thread t1 = new Thread(producer);
        t1.setDaemon(true);
        Thread t2 = new Thread(producer);
        t2.setDaemon(true);
        Thread t3 = new Thread(producer);
        t3.setDaemon(true);
        Thread t4 = new Thread(producer);
        t4.setDaemon(true);
        Thread t5 = new Thread(producer);
        t5.setDaemon(true);

        Thread t6 = new Thread(consumer);
        t6.setDaemon(true);
        Thread t7 = new Thread(consumer);
        t7.setDaemon(true);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();

        t6.start();
        t7.start();

        for ( int i =0; i<100; i++)
        {
            try {
                System.out.println(resultQueue.get());
            }
            catch (InterruptedException ex)
            {}
        }
    }
}
