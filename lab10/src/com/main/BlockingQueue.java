package com.main;

import java.util.*;

public class BlockingQueue
{
    private List _queue = new LinkedList();
    private int _limit = 10;

    public BlockingQueue(int limit){
        this._limit = limit;
    }


    public synchronized void put(Object item)  throws InterruptedException  {
        while(this._queue.size() == this._limit) {
            wait();
        }
        if(this._queue.size() == 0) {
            // wake up any blocked dequeue
            notifyAll();
        }
        this._queue.add(item);
    }


    public synchronized Object get()  throws InterruptedException{
        while(this._queue.size() == 0){
            wait();
        }
        if(this._queue.size() == this._limit){
            // wake up any blocked enqueue
            notifyAll();
        }

        return this._queue.remove(0);
    }
}
