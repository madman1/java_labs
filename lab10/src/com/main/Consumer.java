package com.main;

public class Consumer implements Runnable{

    private BlockingQueue _queue = null;
    private BlockingQueue _resultQueue = null;

    public Consumer(BlockingQueue queue, BlockingQueue resultQueue) {
        _queue = queue;
        _resultQueue = resultQueue;
    }

    public void run() {
        try {

            long n = Thread.currentThread().getId();
            while (true)
            {
                _resultQueue.put("Потік №" + n + " переклав повідомлення -> " +_queue.get());
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
