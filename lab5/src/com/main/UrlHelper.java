package com.main;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.print.Doc;
import java.net.*;
import java.io.*;
import java.util.*;

public class UrlHelper
{
    public static Map<String, Integer> GetTags(Document doc)
    {
        Elements elements  = doc.getAllElements();

        Map<String, Integer> tags = new HashMap<String, Integer>();
        for(Element el : elements)
        {
            String tagName = el.tagName();
            if(tagName.equals("#root"))
            {
                continue;
            }

            if(!tags.containsKey(tagName))
            {
                tags.put(tagName, 1);
            }
            else
            {
                Integer count = tags.get(tagName);
                tags.put(tagName, count + 1);
            }

        }

        return tags;
    }

    public static Document GetDocument(String url) throws IOException
    {
        return Jsoup.connect(url).get();
    }

    public static Map<String, Integer> SortTagsByName(Map<String, Integer> tags)
    {
        List<Map.Entry<String, Integer>> list =
                new LinkedList<Map.Entry<String, Integer>>(tags.entrySet());
        Collections.sort( list, (o1, o2) -> (o1.getKey()).compareTo( o2.getKey() ));

        Map<String, Integer> tagResults = new LinkedHashMap<String, Integer>();
        for(Map.Entry<String, Integer> entry : list)
        {
            tagResults.put(entry.getKey(), entry.getValue());
        }

        return tagResults;
    }

    public static Map<String, Integer> SortTagsByTagFrequency(Map<String, Integer> tags)
    {
        List<Map.Entry<String, Integer>> list =
                new LinkedList<Map.Entry<String, Integer>>(tags.entrySet());
        Collections.sort( list, new Comparator<Map.Entry<String, Integer>>()
        {
            public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )
            {
                return (o1.getValue()).compareTo( o2.getValue() );
            }
        });

        Map<String, Integer> tagResults = new LinkedHashMap<String, Integer>();
        for(Map.Entry<String, Integer> entry : list)
        {
            tagResults.put(entry.getKey(), entry.getValue());
        }

        return tagResults;
    }

    /*public static String GetText(String url) throws IOException {
        try
        {
            URL website = new URL(url);
            URLConnection connection = website.openConnection();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            connection.getInputStream()));

            StringBuilder response = new StringBuilder();
            String inputLine;

            while ((inputLine = in.readLine()) != null)
                response.append(inputLine);

            in.close();


        } catch (Exception e)
        {
            throw e;
        }

        return "";
    }*/
}
