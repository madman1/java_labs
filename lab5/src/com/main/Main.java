package com.main;

import java.io.Console;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws IOException {

        //UrlHelper.GetText("http://stackoverflow.com/questions/40071250/typescript-call-instance-method");
        String url = "http://stackoverflow.com/questions/40071250/typescript-call-instance-method";

        Map<String, Integer> tags = UrlHelper.GetTags(UrlHelper.GetDocument(url));
        Map<String, Integer> byNames = UrlHelper.SortTagsByName(tags);
        Map<String, Integer> byValues = UrlHelper.SortTagsByTagFrequency(tags);

        System.out.print("Sort by tag names: ");
        System.out.print("\n");
        printMap(byNames);

        System.out.print("\n");
        System.out.print("Sort by tag frequency: ");
        System.out.print("\n");
        System.out.print("");
        printMap(byValues);
    }

    private static void printMap(Map<String, Integer> map)
    {
        map.forEach((String x, Integer y) -> System.out.println(x + "   - " + y));
    }

}
