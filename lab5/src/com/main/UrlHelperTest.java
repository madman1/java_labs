package com.main;


import jdk.internal.org.xml.sax.InputSource;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import javax.print.Doc;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.net.*;
import java.io.*;
import java.util.*;

import java.io.StringReader;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by taras_000 on 17-Oct-16.
 */
public class UrlHelperTest
{
    @Test
    public void getTags() throws Exception
    {
        String s = "<!DOCTYPE HTML>\n" +
                "<html>\n" +
                "<head>\n" +
                "<meta charset=\"utf-8\">\n" +
                "<title>Crypto</title>\n" +
                "</head>\n" +
                "<body>\n" +
                " <script type=\"text/javascript\" language =\"javascript\"></script>\n" +
                "    <form name=\"MAIN\" method=POST>\n" +
                "   <select name=\"ACTION\">\n" +
                "  <option disabled>Select action</option>\n" +
                "  <option value=\"encrypt\">encrypt</option>\n" +
                "  <option value=\"decrypt\">decrypt</option>\n" +
                "  </select>\n" +
                "        <input type=\"string\" name=\"STEP\" required>\n" +
                "        <input type=\"submit\" >\n" +
                "    </form>\n" +
                "</body>\n" +
                "</html>";


        Document doc = Jsoup.parse(s);
        Map<String, Integer> tags = UrlHelper.GetTags(doc);

        assertTrue(tags.size() == 10);
        assertTrue(tags.get("option") == 3);
        assertTrue(tags.get("input") == 2);
    }

    @org.junit.Test
    public void sortTagsByName() throws Exception
    {
        Map<String, Integer> tags = new HashMap<String, Integer>();
        tags.put("form", 1);
        tags.put("a", 2);
        tags.put("aside", 3);
        tags.put("input", 6);
        tags.put("h2", 2);
        tags.put("h1", 3);

        Map<String, Integer> sorted = UrlHelper.SortTagsByName(tags);

        int i = 0;
        for (Map.Entry<String, Integer> entry : sorted.entrySet())
        {
            if (i == 0)
            {
                assertTrue(entry.getKey() == "a");
            }
            else if(i == 1)
            {
                assertTrue(entry.getKey() == "aside");
            }
            else  if(i == 5)
            {
                assertTrue(entry.getKey() == "input");
            }

            i++;
        }
    }

    @org.junit.Test
    public void sortTagsByTagFrequency() throws Exception
    {
        Map<String, Integer> tags = new HashMap<String, Integer>();
        tags.put("form", 1);
        tags.put("a", 2);
        tags.put("aside", 3);
        tags.put("input", 6);
        tags.put("h2", 2);
        tags.put("h1", 3);

        Map<String, Integer> sorted = UrlHelper.SortTagsByTagFrequency(tags);

        int i = 0;
        for (Map.Entry<String, Integer> entry : sorted.entrySet())
        {
            if (i == 0)
            {
                assertTrue(entry.getKey() == "form");
            }
            else if(i == 1)
            {
                assertTrue(entry.getKey() == "a");
            }
            else  if(i == 5)
            {
                assertTrue(entry.getKey() == "input");
            }

            i++;
        }
    }

}