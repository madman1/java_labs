package com.main;

import jdk.nashorn.internal.ir.annotations.Immutable;

import java.util.DoubleSummaryStatistics;
import java.util.Objects;

public class Person
{
    private String _name;
    private String _surname;
    private int _age;
    private double _height;
    private double _weight;
    private Gender _gender;

    public  Person()
    {}

    public Person(String name, String surname, int age, Gender g)
    {
        _name = name;
        _surname = surname;
        _age = age;
        this._gender = g;
    }

    public String getName() { return this._name; }
    public void setName(String name) { this._name = name; }

    public String getSurname() { return this._surname; }
    public void setSurname(String surname) { this._surname = surname; }

    public int getAge() { return this._age; }
    public void setAge(int age) { this._age = age; }

    public double getHeight() { return this._height; }
    public void setHeight(double height) { this._height = height; }

    public double getWeight() { return this._weight; }
    public void setWeight(double weight) { this._weight = weight; }

    public Gender getGender() { return this._gender; }
    //public void setGender(Gender gender) { this._gender = gender; }

    @Override
    public  boolean equals(Object other)
    {
        if (other == null || this == null)
            return false;
        if (other == this)
            return true;
        if (!(other.getClass().equals(this.getClass())))
            return false;

        Person person = (Person)other;

        return (this._name == null ? person._name == null : Objects.equals(this._name, person._name)) &&
                (this._surname == null ? person._surname == null : Objects.equals(this._surname, person._surname)) &&
                (person._age == this._age) &&
                (Double.compare(person._height, this._height) == 0) &&
                (Double.compare(person._weight, this._weight) == 0) &&
                (this._gender == null ? person._gender == null : Objects.equals(this._gender, person._gender));
    }

    @Override
    public int hashCode() {
        return Objects.hash(_name, _surname, _age, _height, _weight, _gender);
    }
}
