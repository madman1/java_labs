package com.main;

import com.google.gson.Gson;

public class Main {

    // to add lib file -> project structure -> libraries -> add->from maven
    public static void main(String[] args) {
        Gson gson = new Gson();
        Person p = new Person("Ivan", "Barbul", 33, Gender.Male);
        p.setHeight(1.8);
        p.setWeight(65);

        String json = gson.toJson(p);
        Person obj = gson.fromJson(json, Person.class);

        boolean t = obj.equals(p);
    }

    // equals verifier , transition симитричность транзитивность рефликсивность
}
