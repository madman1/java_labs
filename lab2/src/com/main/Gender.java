package com.main;

import jdk.nashorn.internal.ir.annotations.Immutable;

@Immutable
public enum  Gender
{
    Male,
    Female
}
