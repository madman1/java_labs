package com.main;

import com.google.gson.Gson;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PersonTest {

    private Person _person;
    private Person _personFromJson;
    Gson _gson = new Gson();

    @Before
    public  void SetDefaults()
    {
        _person = new Person("Ivan", "Barbul", 33, Gender.Male);
        _person.setHeight(1.8);
        _person.setWeight(65);

        _gson = new Gson();
        String json = _gson.toJson(_person);
        _personFromJson = _gson.fromJson(json, Person.class);
    }

    @Test
    public void equalsTrue()
    {
        // arrange

        // act
        boolean t = _personFromJson.equals(_person);

        //assert
        assertTrue(t);
    }

    @Test
    public void equalsFalse()
    {
        // arrange
        _person.setAge(12);

        // act
        boolean t = _personFromJson.equals(_person);

        //assert
        assertFalse(t);
    }

    @Test
    public void equalsFalseNull()
    {
        // act
        boolean t = _person.equals(null);

        //assert
        assertFalse(t);
    }

    @Test
    public void equalsFalseString() {
        // act
        boolean t = _person.equals("Ivan");

        //assert
        assertFalse(t);
    }

    //Symmetric
    @Test
    public void equalsSymmetric()
    {
        // arrange

        // act
        boolean t = _personFromJson.equals(_person);
        boolean t1 = _person.equals(_personFromJson);

        //assert
        assertTrue(t);
        assertTrue(t1);
    }

    //Transitive
    @Test
    public void equalsTransitive()
    {
        // arrange
        String json = _gson.toJson(_person);
        Person thirdPerson = _gson.fromJson(json, Person.class);

        // act
        boolean t1 = _person.equals(_personFromJson);
        boolean t2 = _personFromJson.equals(thirdPerson);
        boolean t3 = _person.equals(thirdPerson);

        //assert
        assertTrue(t1);
        assertTrue(t2);
        assertTrue(t3);
    }

    //Reflexive
    @Test
    public void equalsReflexive()
    {
        // act
        boolean t = _person.equals(_person);

        //assert
        assertTrue(t);
    }

    @Test
    public void equalsContract()
    {
        EqualsVerifier.forClass(Person.class).usingGetClass().suppress(Warning.NONFINAL_FIELDS).verify();
    }
}