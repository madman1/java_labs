package com.main.animals;


public abstract class Animal
{
    private double _weight;
    private  double _speed;

    public double getWeight() {
        return _weight;
    }

    public void setWeight(double weight) {
        this._weight = weight;
    }

    public double getSpeed() {
        return _speed;
    }

    public void setSpeed(double speed) {
        this._speed = speed;
    }
}
