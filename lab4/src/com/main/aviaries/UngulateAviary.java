package com.main.aviaries;


import com.main.animals.Ungulate;

public class UngulateAviary<T extends Ungulate>  extends  MammalAviary<T>
{
    public UngulateAviary(int capacity)
    {
        super(capacity);
    }
}
