package com.main.aviaries;

import com.main.animals.Bird;

public class BirdAviary<T extends Bird> extends  Aviary<T>
{
    public BirdAviary(int capacity)
    {
        super(capacity);
    }
}
