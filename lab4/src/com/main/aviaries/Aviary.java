package com.main.aviaries;

import com.main.animals.Animal;

import java.util.ArrayList;
import java.util.List;

public abstract class Aviary<T extends Animal>
{
    private List<T> animals = new ArrayList();
    private int _capacity;

    public Aviary(int capacity)
    {
        _capacity = capacity;
    }

    public int getCapacity()
    {
        return _capacity;
    }

    public int getEngaged()
    {
        return  animals.size();
    }

    public void add(T animal)
    {
        if(animals.size() >= _capacity)
        {
            throw  new IllegalArgumentException("Aviary is already full");
        }

        animals.add(animal);
    }

    public  void remove(T animal)
    {
        if(!animals.contains(animal))
        {
            throw  new IllegalArgumentException("Animal doesn`t exist in current aviary");
        }

        animals.remove(animal);
    }
}
