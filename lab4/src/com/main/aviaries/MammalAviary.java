package com.main.aviaries;

import com.main.animals.Mammal;

public class MammalAviary<T extends  Mammal> extends Aviary<T>
{
    public <T extends Mammal> MammalAviary(int capacity)
    {
        super(capacity);
    }
}
