package com.main.aviaries;

import com.main.animals.Lion;

public class LionsAviary extends MammalAviary<Lion>
{
    public LionsAviary(int capacity)
    {
        super(capacity);
    }
}
