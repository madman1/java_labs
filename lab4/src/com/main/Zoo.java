package com.main;

import com.main.animals.Animal;
import com.main.aviaries.Aviary;

import java.util.ArrayList;
import java.util.List;

public class Zoo
{
    public List<Aviary<? extends Animal>> cages = new ArrayList<>();

    public int getCountOfAnimals ()
    {
        int count = 0;
        for(Aviary<? extends Animal> t : cages)
        {
            count += t.getEngaged();
        }

        return count;
    }

    public void addCage(Aviary<? extends Animal> cage)
    {
        cages.add(cage);
    }
}
