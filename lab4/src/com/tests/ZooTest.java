package com.tests;

import com.main.Zoo;
import com.main.animals.*;
import com.main.aviaries.BirdAviary;
import com.main.aviaries.LionsAviary;
import com.main.aviaries.UngulateAviary;
import com.sun.xml.internal.ws.api.streaming.XMLStreamWriterFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.sun.xml.internal.ws.dump.LoggingDumpTube.Position.Before;
import static org.junit.Assert.*;


public class ZooTest
{
    Zoo _zoo;

    @org.junit.Before
    public void setUp()
    {
        _zoo = new Zoo();
    }

    @Test
    public void testUngulate()
    {
        Zebra zebra = new Zebra();
        Giraffe giraffe = new Giraffe();

        UngulateAviary<Ungulate> ungulateCage = new UngulateAviary<>(3);
        ungulateCage.add(zebra);
        ungulateCage.add(giraffe);

        _zoo.addCage(ungulateCage);

        Assert.assertTrue(_zoo.getCountOfAnimals() == 2);
        Assert.assertTrue(ungulateCage.getCapacity() == 3);
        Assert.assertTrue(ungulateCage.getEngaged() == 2);
    }

    @Test
    public void testUngulateThrows() throws IllegalArgumentException
    {
        try {
            Zebra zebra = new Zebra();
            Giraffe giraffe = new Giraffe();
            Zebra zebra1 = new Zebra();

            UngulateAviary<Ungulate> ungulateCage = new UngulateAviary<>(2);
            ungulateCage.add(zebra);
            ungulateCage.add(giraffe);
            ungulateCage.add(zebra1);
            fail();
        }
        catch (Exception e)
        {}
    }

    @Test
    public void testFewAviaries()
    {
        Zebra zebra = new Zebra();
        Giraffe giraffe = new Giraffe();

        UngulateAviary<Ungulate> ungulateCage = new UngulateAviary<>(3);
        ungulateCage.add(zebra);
        ungulateCage.add(giraffe);

        BirdAviary<Eagle> av = new BirdAviary(2);
        av.add(new Eagle());

        _zoo.addCage(ungulateCage);
        _zoo.addCage(av);

        LionsAviary c = new LionsAviary(2);
        c.add(new Lion());

        Assert.assertTrue(_zoo.getCountOfAnimals() == 3);
    }
}