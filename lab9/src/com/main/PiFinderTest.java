package com.main;

import java.text.DecimalFormat;

import static org.junit.Assert.*;

/**
 * Created by taras_000 on 13-Nov-16.
 */
public class PiFinderTest {
    @org.junit.Before
    public void setUp() throws Exception {

    }

    @org.junit.Test
    public void calculate() throws Exception {
        long numberOfIterations = 1000000000;
        double res = PiFinderParallel.Calculate(numberOfIterations, null);

        DecimalFormat df = new DecimalFormat("#.##");
        res = Double.valueOf(df.format(res));

        assertTrue(res == 3.14);
    }

    @org.junit.Test
    public void calculate1() throws Exception
    {
        long numberOfIterations = 1000000000;
        double res = PiFinder.Calculate(numberOfIterations);

        DecimalFormat df = new DecimalFormat("#.##");
        res = Double.valueOf(df.format(res));

        assertTrue(res == 3.14);
    }

}