package com.main;


public class PiFinderConcurrent
{
    private long _overalNumberOfIterations;
    public PiFinderConcurrent(long numberOfIterations)
    {
        _overalNumberOfIterations = numberOfIterations;
    }

    public double Calculate(int numberOfThreads)
    {
        long numberOfIterationsForThread = _overalNumberOfIterations / numberOfThreads;

        PiFinderThread myThreads[] = new PiFinderThread[numberOfThreads];

        for(int i =0; i< numberOfThreads; i++)
        {
            PiFinderThread thread = new PiFinderThread(numberOfIterationsForThread);
            myThreads[i] = thread;
            thread.start();
        }

        try
        {
            for (int j = 0; j < numberOfThreads; j++)
            {
                myThreads[j].join();
            }
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        long success = 0;
        for (int j = 0; j < numberOfThreads; j++)
        {
            success += myThreads[j].getResult();
        }

        double res =  4.0 * success / _overalNumberOfIterations;
        return  res;
    }
}
