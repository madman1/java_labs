package com.main;


public class PiFinderThread extends Thread
{
    private long _numberOfIterations;
    private long _result;

    public PiFinderThread(long numberOfIterations)
    {
        _numberOfIterations = numberOfIterations;
    }

    @Override
    public void run()
    {
        _result = CalculateInternal(_numberOfIterations);
    }

    private static long CalculateInternal(long numberOfIterations)
    {
        long numberOfSuccess = 0;
        double x, y;

        for (int i = 0; i < numberOfIterations ; i++)
        {
            x = Math.random();
            y = Math.random();

            if ( x*x + y*y <= 1 )
                numberOfSuccess++;
        }

        return  numberOfSuccess;
    }

    public long getResult() {
        return _result;
    }
}

