package com.main;


public class PiFinder
{
    public static double Calculate(long numberOfThrows)
    {
        long numberOfSuccess = 0;
        double x, y;

        for (long i = 0; i < numberOfThrows ; i++)
        {
            x = Math.random();
            y = Math.random();

            if ( x*x + y*y <= 1 )
                numberOfSuccess++;
        }

        return 4.0 * numberOfSuccess / numberOfThrows;
    }
}
