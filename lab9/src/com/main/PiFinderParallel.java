package com.main;
import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class PiFinderParallel
{
    private static ThreadLocal<Random> _random = new ThreadLocal<Random>()
    {
        protected Random initialValue()
        {
            return new Random();
        };
    };

    private static int _numberOfCPU = Runtime.getRuntime().availableProcessors();

    public static int GetNumberOfCPU()
    {
        return _numberOfCPU;
    }

    public static double Calculate(long numberOfThrows, Integer numberOfCpu)
    {
        int n = numberOfCpu == null ? GetNumberOfCPU() : numberOfCpu;

        long portion = numberOfThrows / n;

        // add up how many samples appear in the circle
        //long inside = LongStream.range(0, n).parallel().map(s -> CalculatePortion(portion)).sum();

        ForkJoinPool pool = new ForkJoinPool(n);
        long inside = pool.submit(() ->
                LongStream.range(0, n).parallel().map(s -> CalculatePortion(portion)).sum()).join();

        pool.shutdown();

        return (4.0 * inside) / numberOfThrows;
    }

    private static long CalculatePortion(long numberOfIterations) {
        Random random = _random.get();
        long numberOfSuccess = 0;
        double x, y;

        for (int i = 0; i < numberOfIterations ; i++)
        {
            x = random.nextDouble();
            y = random.nextDouble();

            if ( x*x + y*y <= 1 )
                numberOfSuccess++;
        }

        return numberOfSuccess;
    }
}
