package com.main;

import java.util.*;

public class Main {


    // fork join pull for thread pull in PiFinderParallel
    // github.com/sirotae/javacore-kpi

    // part2 parallel
    public static void main(String[] args) {

        long startTime = System.currentTimeMillis();
        long numberOfIterations = 1000000000;

        //System.out.println("PI IS " +PiFinder.Calculate(numberOfIterations));
        //System.out.println("THREADS   " + 1);

        //PiFinderConcurrent finder = new PiFinderConcurrent(numberOfIterations);
        //System.out.println("PI IS " +finder.Calculate(4));
        //System.out.println("THREADS   " + 4);

        System.out.println("PI IS " + PiFinderParallel.Calculate(numberOfIterations, null));
        System.out.println("THREADS " + PiFinderParallel.GetNumberOfCPU());

        System.out.println("ITERATIONS " + numberOfIterations);
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("TIME " + elapsedTime);
    }
}
