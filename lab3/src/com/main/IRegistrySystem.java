package com.main;

/**
 * Created by taras_000 on 16-Oct-16.
 */
public interface IRegistrySystem
{
    void setOnePassPrice(int onePassPrice);
    int getOnePassPrice();
}
