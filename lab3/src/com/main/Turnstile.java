package com.main;

import com.main.Cards.*;
import com.main.Enums.Term;

import java.util.Calendar;
import java.util.Date;

public class Turnstile
{
    private  RegistrySystem _registrySystem;

    public Turnstile(RegistrySystem system)
    {
        _registrySystem = system;
    }

    public boolean TryToPass(ICard card)
    {
        return card.TryToPass();
    }

    public String GetInfo(ICard card)
    {
        return card.getInfo();
    }
}
