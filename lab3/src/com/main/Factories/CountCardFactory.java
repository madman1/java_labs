package com.main.Factories;

import com.main.Cards.Card;
import com.main.Cards.CountCard;
import com.main.Enums.CardType;
import com.main.IRegistrySystem;

public class CountCardFactory implements  ICardFactory
{
    private CardType _type;
    private int _count;

    public CountCardFactory(CardType type, int count)
    {
        _type = type;
        _count = count;
    }

    @Override
    public Card createCard(IRegistrySystem registrySystem)
    {
        return new CountCard(registrySystem, _type, _count);
    }

    public void setCount(int count)
    {
        if(_count <= 0)
        {
            throw new IllegalArgumentException("count");
        }

        _count = count;
    }
}
