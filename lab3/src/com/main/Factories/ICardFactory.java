package com.main.Factories;

import com.main.Cards.Card;
import com.main.IRegistrySystem;

public interface ICardFactory
{
    Card createCard(IRegistrySystem registrySystem);
}
