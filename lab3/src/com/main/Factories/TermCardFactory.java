package com.main.Factories;


import com.main.Cards.Card;
import com.main.Cards.TermCard;
import com.main.Enums.*;
import com.main.IRegistrySystem;

public class TermCardFactory implements  ICardFactory
{
    private CardType _type;
    private Term _term;

    public  TermCardFactory(CardType type, Term term)
    {
        _type = type;
        _term = term;
    }

    @Override
    public Card createCard(IRegistrySystem registrySystem)
    {
        return new TermCard(registrySystem, _type, _term);
    }
}
