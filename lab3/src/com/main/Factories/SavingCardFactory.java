package com.main.Factories;

import com.main.Cards.Card;
import com.main.Cards.CountCard;
import com.main.Cards.SavingCard;
import com.main.Enums.CardType;
import com.main.IRegistrySystem;

public class SavingCardFactory implements ICardFactory
{
    private int _sum;

    public SavingCardFactory(int sum)
    {
        _sum = sum;
    }

    @Override
    public Card createCard(IRegistrySystem registrySystem)
    {
        return new SavingCard(registrySystem, _sum);
    }

    public void setSum(int sum)
    {
        if(sum <= 0)
        {
            throw new IllegalArgumentException("sum");
        }

        _sum = sum;
    }
}
