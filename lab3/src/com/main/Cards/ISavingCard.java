package com.main.Cards;

public interface ISavingCard extends ICard
{
    void  setSum(int sum);
    int getSum();
    void  addMoney(int money);
    void withdrawMoney(int money);
}
