package com.main.Cards;

import com.main.Enums.CardType;

import java.util.UUID;

public interface ICard
{
    UUID getId();
    CardType getCardType();
    int getNumberOfPasses();
    int getNumberOfRejections();
    void increaseNumberOfPasses();
    void increaseNumberOfRejections();
    String getInfo();

    boolean TryToPass();
}
