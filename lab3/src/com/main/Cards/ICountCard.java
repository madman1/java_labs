package com.main.Cards;

public interface ICountCard extends ICard
{
    void setCount(int count);
    int getCount();
    void decreaseCount();
}
