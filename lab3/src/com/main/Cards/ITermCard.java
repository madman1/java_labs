package com.main.Cards;


import com.main.Enums.Term;

import java.util.Calendar;

public interface ITermCard extends ICard
{
    void  setTerm(Term term);
    Term getTerm();
    Calendar getInitialDate();
    Calendar getTillDate();
}

