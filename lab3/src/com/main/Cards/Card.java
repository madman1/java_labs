package com.main.Cards;

import com.main.Enums.CardType;
import com.main.IRegistrySystem;

import java.util.UUID;

public abstract class Card
{
    IRegistrySystem _registrySystem;
    private  UUID _id;
    private CardType _cardType;
    private int _numberOfPasses;
    private int _numberOfRejections;

    public Card(IRegistrySystem registrySystem, CardType cardType)
    {
        _registrySystem = registrySystem;
        _id = UUID.randomUUID();
        _numberOfPasses = 0;
        _numberOfRejections = 0;
        _cardType = cardType;
    }

    public UUID getId()
    {
        return  _id;
    }

    public  CardType getCardType()
    {
        return  _cardType;
    }

    public int getNumberOfPasses()
    {
        return  _numberOfPasses;
    }

    public int getNumberOfRejections()
    {
        return  _numberOfRejections;
    }

    public void  increaseNumberOfPasses()
    {
        _numberOfPasses ++;
    }

    public void  increaseNumberOfRejections()
    {
        _numberOfRejections ++;
    }

    public String getInfo()
    {
        return  "Card id: " + _id +
                "\nCard type: " + _cardType.toString() +
                "\nNumber of passes: " + _numberOfPasses +
                "\nNumber of rejections: " + _numberOfRejections;

    }
}
