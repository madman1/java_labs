package com.main.Cards;
import com.main.Enums.*;
import com.main.IRegistrySystem;

public class CountCard extends Card implements ICountCard
{
    private  int _count;

    public CountCard(IRegistrySystem registrySystem, CardType type, int count)
    {
        super(registrySystem, type);
        _count = count;
    }

    @Override
    public void setCount(int count)
    {
        if(count <0)
        {
            throw new IllegalArgumentException("count");
        }

        _count = count;
    }

    @Override
    public int getCount()
    {
        return _count;
    }

    @Override
    public void decreaseCount()
    {
        _count --;
    }

    @Override
    public String getInfo()
    {
        return  super.getInfo() +
                "\nNumber of available passes: " + _count;

    }

    public boolean TryToPass()
    {
        if(_count <= 0)
        {
            this.increaseNumberOfRejections();
            return false;
        }

        this.increaseNumberOfPasses();
        this.decreaseCount();
        return  true;
    }
}
