package com.main.Cards;

import com.main.Enums.CardType;
import com.main.IRegistrySystem;

public class SavingCard extends Card implements ISavingCard
{
    private int _sum;
    public SavingCard(IRegistrySystem registrySystem, int sum)
    {
        super(registrySystem, CardType.Simple);
        _sum = sum;
    }

    @Override
    public void setSum(int sum)
    {
        if(sum <0)
        {
            throw new IllegalArgumentException("sum");
        }
        _sum = sum;
    }

    @Override
    public int getSum()
    {
        return _sum;
    }

    @Override
    public void addMoney(int money)
    {
        if(money <0)
        {
            throw new IllegalArgumentException("money");
        }
        _sum += money;
    }

    @Override
    public void withdrawMoney(int money)
    {
        if(money <0)
        {
            throw new IllegalArgumentException("money");
        }

        _sum -= money;
    }

    @Override
    public String getInfo()
    {
        return  super.getInfo() +
                "\nAvailable balance: " + _sum;

    }

    public boolean TryToPass()
    {
        int sum = this.getSum();
        int minimalSum = _registrySystem.getOnePassPrice();

        if(sum < minimalSum)
        {
            this.increaseNumberOfRejections();
            return false;
        }

        this.increaseNumberOfPasses();
        this.withdrawMoney(minimalSum);
        return true;
    }
}
