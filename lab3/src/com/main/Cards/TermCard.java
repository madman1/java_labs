package com.main.Cards;


import com.main.Enums.*;
import com.main.IRegistrySystem;
import com.main.RegistrySystem;

import java.util.Calendar;
import java.util.Date;

public class TermCard extends Card implements ITermCard
{
    private Term _term;
    private Calendar _initialDate;

    public  TermCard(IRegistrySystem registrySystem, CardType type, Term term)
    {
        super(registrySystem, type);
        _term = term;
        _initialDate = Calendar.getInstance();
    }

    @Override
    public void setTerm(Term term)
    {
        _term = term;
    }

    @Override
    public Term getTerm()
    {
        return _term;
    }

    @Override
    public Calendar getInitialDate()
    {
        return _initialDate;
    }

    public Calendar getTillDate()
    {
        Calendar initialDate = getInitialDate();
        Term validTerm = getTerm();

        Calendar tillDate = Calendar.getInstance();
        tillDate.setTime(initialDate.getTime());
        if(validTerm == Term.Month)
        {
            tillDate.add(Calendar.MONTH, 1);
        }

        if(validTerm == Term.TenDays)
        {
            tillDate.add(Calendar.DATE, 10);
        }

        return tillDate;
    }

    @Override
    public String getInfo()
    {
        return  super.getInfo() +
                "\nInitial date: " + getInitialDate() +
                "\nCard is available till: " + getTillDate();

    }

    public boolean TryToPass()
    {
        Calendar tillDate = this.getTillDate();
        Calendar currentDate = Calendar.getInstance();

        if(currentDate.compareTo(tillDate) == 1)
        {
            this.increaseNumberOfRejections();
            return false;
        }

        this.increaseNumberOfPasses();
        return  true;
    }
}
