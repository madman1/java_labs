package com.main;


import com.main.Cards.Card;
import com.main.Factories.ICardFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


public class RegistrySystem implements IRegistrySystem
{
    private  int _onePassPrice;
    private  static Map<UUID, Card> _cards = new HashMap<UUID, Card>();

    public RegistrySystem(int onePassPrice)
    {
        _onePassPrice = onePassPrice;
    }

    public Card ReleaseCard(ICardFactory factory)
    {
        Card card = factory.createCard(this);
        _cards.put(card.getId(), card);

        return  card;
    }

    public void setOnePassPrice(int onePassPrice)
    {
        _onePassPrice = onePassPrice;
    }

    public int getOnePassPrice()
    {
        return _onePassPrice;
    }
}

