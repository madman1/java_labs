package com.tests;

import com.main.Cards.ITermCard;
import com.main.Enums.CardType;
import com.main.Enums.Term;
import com.main.Factories.ICardFactory;
import com.main.Factories.TermCardFactory;
import com.main.RegistrySystem;
import com.main.Turnstile;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;


public class TermCardTest
{
    private ITermCard _termCard;
    private Turnstile _turnStile;

    @Before
    public void beforeClass()
    {
        RegistrySystem r = new RegistrySystem(5);
        ICardFactory factory = new TermCardFactory(CardType.Pupil, Term.TenDays);
        _termCard = (ITermCard) r.ReleaseCard(factory);
        _turnStile = new Turnstile(r);
    }

    @Test
    public void allAvailable()
    {
        Calendar t = _termCard.getTillDate();
        int passes = _termCard.getNumberOfPasses();
        int rejections = _termCard.getNumberOfRejections();
        CardType type = _termCard.getCardType();

        String temp = _turnStile.GetInfo(_termCard);

        Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.DATE, 10);

        Assert.assertTrue(t.get(Calendar.DATE) == currentDate.get(Calendar.DATE));
        Assert.assertTrue(passes == 0);
        Assert.assertTrue(rejections == 0);
        Assert.assertTrue(type == CardType.Pupil);
    }

    @Test
    public void fewPasses()
    {
        for (int i=0; i<100; i++)
        {
            _turnStile.TryToPass(_termCard);
        }

        int passes = _termCard.getNumberOfPasses();
        int rejections = _termCard.getNumberOfRejections();
        CardType type = _termCard.getCardType();

        String temp = _turnStile.GetInfo(_termCard);

        Assert.assertTrue(passes == 100);
        Assert.assertTrue(rejections == 0);
        Assert.assertTrue(type == CardType.Pupil);
    }
}