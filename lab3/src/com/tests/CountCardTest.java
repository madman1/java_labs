package com.tests;

import com.main.Cards.Card;
import com.main.Cards.ICountCard;
import com.main.Enums.CardType;
import com.main.Factories.CountCardFactory;
import com.main.Factories.ICardFactory;
import com.main.RegistrySystem;
import com.main.Turnstile;
import org.junit.*;

import static org.junit.Assert.*;


public class CountCardTest extends Assert {

    private ICountCard _countCard;
    private Turnstile _turnStile;

    @Before
    public void beforeClass()
    {
        RegistrySystem r = new RegistrySystem(5);
        ICardFactory factory = new CountCardFactory(CardType.Pupil, 5);
        _countCard = (ICountCard) r.ReleaseCard(factory);
        _turnStile = new Turnstile(r);
    }

    @Test
    public void allAvailable()
    {
        int count = _countCard.getCount();
        int passes = _countCard.getNumberOfPasses();
        int rejections = _countCard.getNumberOfRejections();
        CardType type = _countCard.getCardType();

        String temp = _turnStile.GetInfo(_countCard);

        Assert.assertTrue(count == 5);
        Assert.assertTrue(passes == 0);
        Assert.assertTrue(rejections == 0);
        Assert.assertTrue(type == CardType.Pupil);
    }

    @Test
    public void fewPasses()
    {
        _turnStile.TryToPass(_countCard);
        _turnStile.TryToPass(_countCard);

        int count = _countCard.getCount();
        int passes = _countCard.getNumberOfPasses();
        int rejections = _countCard.getNumberOfRejections();
        CardType type = _countCard.getCardType();

        String temp = _turnStile.GetInfo(_countCard);

        Assert.assertTrue(count == 3);
        Assert.assertTrue(passes == 2);
        Assert.assertTrue(rejections == 0);
        Assert.assertTrue(type == CardType.Pupil);
    }

    @Test
    public void fewRejections()
    {
        _turnStile.TryToPass(_countCard);
        _turnStile.TryToPass(_countCard);
        _turnStile.TryToPass(_countCard);
        _turnStile.TryToPass(_countCard);
        _turnStile.TryToPass(_countCard);
        _turnStile.TryToPass(_countCard);
        _turnStile.TryToPass(_countCard);
        _turnStile.TryToPass(_countCard);

        int count = _countCard.getCount();
        int passes = _countCard.getNumberOfPasses();
        int rejections = _countCard.getNumberOfRejections();
        CardType type = _countCard.getCardType();

        String temp = _turnStile.GetInfo(_countCard);

        Assert.assertTrue(count == 0);
        Assert.assertTrue(passes == 5);
        Assert.assertTrue(rejections == 3);
        Assert.assertTrue(type == CardType.Pupil);
    }
}