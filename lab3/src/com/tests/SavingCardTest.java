package com.tests;

import com.main.Cards.ICountCard;
import com.main.Cards.ISavingCard;
import com.main.Enums.CardType;
import com.main.Factories.CountCardFactory;
import com.main.Factories.ICardFactory;
import com.main.Factories.SavingCardFactory;
import com.main.RegistrySystem;
import com.main.Turnstile;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SavingCardTest
{
    private ISavingCard _savingCard;
    private Turnstile _turnStile;

    @Before
    public void beforeClass()
    {
        RegistrySystem r = new RegistrySystem(5);
        ICardFactory factory = new SavingCardFactory(25);
        _savingCard = (ISavingCard) r.ReleaseCard(factory);
        _turnStile = new Turnstile(r);
    }

    @Test
    public void allAvailable()
    {
        int count = _savingCard.getSum();
        int passes = _savingCard.getNumberOfPasses();
        int rejections = _savingCard.getNumberOfRejections();
        CardType type = _savingCard.getCardType();

        String temp = _turnStile.GetInfo(_savingCard);

        Assert.assertTrue(count == 25);
        Assert.assertTrue(passes == 0);
        Assert.assertTrue(rejections == 0);
        Assert.assertTrue(type == CardType.Simple);
    }

    @Test
    public void fewPasses()
    {
        _turnStile.TryToPass(_savingCard);
        _turnStile.TryToPass(_savingCard);

        int count = _savingCard.getSum();
        int passes = _savingCard.getNumberOfPasses();
        int rejections = _savingCard.getNumberOfRejections();
        CardType type = _savingCard.getCardType();

        String temp = _turnStile.GetInfo(_savingCard);

        Assert.assertTrue(count == 15);
        Assert.assertTrue(passes == 2);
        Assert.assertTrue(rejections == 0);
        Assert.assertTrue(type == CardType.Simple);
    }

    @Test
    public void fewRejections()
    {
        _turnStile.TryToPass(_savingCard);
        _turnStile.TryToPass(_savingCard);
        _turnStile.TryToPass(_savingCard);
        _turnStile.TryToPass(_savingCard);
        _turnStile.TryToPass(_savingCard);
        _turnStile.TryToPass(_savingCard);
        _turnStile.TryToPass(_savingCard);
        _turnStile.TryToPass(_savingCard);

        int count = _savingCard.getSum();
        int passes = _savingCard.getNumberOfPasses();
        int rejections = _savingCard.getNumberOfRejections();
        CardType type = _savingCard.getCardType();

        String temp = _turnStile.GetInfo(_savingCard);

        Assert.assertTrue(count == 0);
        Assert.assertTrue(passes == 5);
        Assert.assertTrue(rejections == 3);
        Assert.assertTrue(type == CardType.Simple);
    }
}