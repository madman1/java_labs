package com.main;

import java.util.*;

/**
 * Detects prime numbers
 */
public class PrimeNumberDetector
{
    /**
     * Returns prime int number with the max number of 1 in binary form
     * @param maxNumber max number of the sequence
     * @return prime int
     */
    public static int GetPrimeByOnes(int maxNumber)
    {
        if(maxNumber <= 1)
        {
            throw new IllegalArgumentException("maxNumber is lower than the lowes pime number");
        }

        List<Integer> primeNumbers = GetPrimeNumbers(maxNumber);

        int countOfOnes = 1;
        int result = 2;

        for (Integer numb : primeNumbers)
        {
            int count = Integer.toBinaryString(numb).replace("0", "").length();

            if(count > countOfOnes)
            {
                result = numb;
                countOfOnes = count;
            }
        }

        return  result;
    }

    /**
     * Returns list of prime numbers from the sequence using Sieve of Eratosthenes algorithm
     * @param maxNumber max number of the sequence
     * @return list of prime numbers
     */
    private static List<Integer> GetPrimeNumbers(int maxNumber)
    {
        if(maxNumber <= 1)
        {
            return new ArrayList<Integer>();
        }

        boolean[] allNumbers = new boolean[maxNumber+1];

        for(int i= 2; i<= maxNumber; i++)
        {
            allNumbers[i] = true;
        }

        for(int i = 2; i<= maxNumber; i++)
        {
            if(Math.pow(i, 2) > maxNumber)
            {
                break;
            }

            if(allNumbers[i] == true)
            {
                for (int j = (int)Math.pow(i, 2); j<=maxNumber; j+=i)
                {
                    allNumbers[j] = false;
                }
            }
        }

        List<Integer> result = new ArrayList<Integer>();
        for(int i = 2; i<= maxNumber; i++)
        {
            if(allNumbers[i] == true)
            {
                result.add(i);
            }
        }

        return result;
    }
}
