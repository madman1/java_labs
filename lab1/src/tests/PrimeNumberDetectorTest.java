package tests;

import com.main.PrimeNumberDetector;
import org.junit.Test;

import static org.junit.Assert.*;

public class PrimeNumberDetectorTest
{
    @Test
    public void getPrimeByOnesWithEqualVariants()
    {
        int result = PrimeNumberDetector.GetPrimeByOnes(18);

        // n=18 result = 13 or 7 or 11, cause  17 - 10001, 13 - 1101, 7 - 111, 11 - 1011
        boolean actual = result == 13 || result == 7 || result == 11;
        assertTrue(actual);
    }

    @Test
    public void getPrimeByOnesWithOneVariant()
    {
        int result = PrimeNumberDetector.GetPrimeByOnes(7);

        //n=7 result = 7
        assertEquals(result, 7);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getPrimeByOnesWithNegative()
    {
            PrimeNumberDetector.GetPrimeByOnes(-5);
    }

    @Test
    public void getPrimeByOnesWithOne() throws IllegalArgumentException
    {
        try {
            PrimeNumberDetector.GetPrimeByOnes(1);
            fail();
        }
        catch (IllegalArgumentException e)
        {
            assertEquals(e.getMessage(), "maxNumber is lower than the lowes pime number");
        }
    }

    @Test
    public void getPrimeByOnesWithZero() throws IllegalArgumentException
    {
        try {
            PrimeNumberDetector.GetPrimeByOnes(0);
            fail();
        }
        catch (IllegalArgumentException e)
        {
            assertEquals(e.getMessage(), "maxNumber is lower than the lowes pime number");
        }
    }
}